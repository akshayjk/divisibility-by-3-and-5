console.clear(); // clear you console so that your result appear from starting

let i = 0;

do {
    i += 1;
    if (i % 3 == 0 && i % 5 == 0) console.log('FizzBuzz --', i); // Numbers divisible by both three & five are shown FizzBuzz 
    else if (i % 3 == 0) console.log('Fizz --', i); // Only numbers divisible by Three are shown Fizz
    else if (i % 5 == 0) console.log('Buzz --', i); // Only numbers divisible by Five are shown Buzz
    else console.log(i);
} while (i < 100);